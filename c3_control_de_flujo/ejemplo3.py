# Este ejmplo muestra el uso de for
# para acceder estructuras de datos
# mas complejas.

# La variable estudiante es una lista
# que contiene diccionarios que describen
# el nombre y la edad de los estudiantes
estudiantes = [
    {"nombre": "Mario", "edad": 34 },
    {"nombre": "Ana", "edad": 28 },
    {"nombre": "Gabriela", "edad": 25 }
    ]

# La siguiente iteración muestra el nombre
# y la edad para cada uno de los estudiantes
# También muestra el promedio de edades
# para todos los estudiantes
suma_edades = 0
for estudiante in estudiantes:
    print("Nombre: "+estudiante["nombre"])
    print("Edad: "+str(estudiante["edad"]))
    suma_edades += estudiante["edad"]

print("Edad promedio: ")
edad_promedio = suma_edades/len(estudiantes)
print(str(edad_promedio))
