# Este ejemplo calcula las edades promedio
# ingresadas. El programa se repite de forma
# indefinida hasta que el usuario ingresa
# la palabra "salir"
suma_edades = 0
conteo_edades = 0
continuar = True

while continuar:
    edad = input("Ingrese una edad: ")
    
    # Si el usuario ingresa el texto
    # "salir". Rompemos el ciclo con
    # la palabra *break*
    if edad=="salir":
        print("Saliendo...")
        break

    # Para cada valor ingresado
    edad = int(edad)
    suma_edades += edad
    conteo_edades += 1
    edad_promedio = suma_edades/conteo_edades
    print("Edad promedio: ")
    print(str(edad_promedio))
