# Este programa indica al usuario si puede
# trabajar, si es mayor de edad si está
# en edad de jubilación e indica que no
# puede trabajar para los menores de edad
edad = input("Por favor. Ingrese su edad: ")
edad = int(edad)
# Si esta entre 16 y menos de 18 años puede trabajar
if edad >=16 and edad < 18:
    print("Puede trabajar con restricciones")
elif edad >=18:
    print("Puede trabajar")
    # Si es mayor de 65 años se puede jubilar
    if edad >= 65:
        print("Se puede jubilar")
else:
    # Si es menor de 16 entonces no pueda trabajar
    print("Usted no puede trabajar")


