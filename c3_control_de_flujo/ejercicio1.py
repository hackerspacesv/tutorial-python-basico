# Tarea: Modifica que programa para que 
# pregunte por el sexo de la persona y
# muestre el mensaje de si se puede
# jubilar a los 60 años en caso de que el sexo
# sea mujer.
# Se espera que el usuario puede ingresar
# las palabras "hombre" y "mujer"
edad = input("Por favor. Ingrese su edad: ")
edad = int(edad)
# Si esta entre 16 y menos de 18 años puede trabajar
if edad >=16 and edad < 18:
    print("Puede trabajar con restricciones")
elif edad >=18:
    print("Puede trabajar")
    # Si es mayor de 65 años se puede jubilar
    if edad >= 65:
        print("Se puede jubilar")
else:
    # Si es menor de 16 entonces no pueda trabajar
    print("Usted no puede trabajar")


