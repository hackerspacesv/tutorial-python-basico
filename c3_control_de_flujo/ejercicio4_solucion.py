# Este ejemplo calcula las edades promedio
# ingresadas. El programa se repite de forma
# indefinida hasta que el usuario ingresa
# la palabra "salir"

# Tarea: Modifique el programa para que
# muestre la edad máxima y la edad
# mínima ingresada. No se consid
suma_edades = 0
conteo_edades = 0
continuar = True

# Variables que guardan los máximos y mínimos
edad_maxima = 0
edad_minima = 0

while continuar:
    edad = input("Ingrese una edad: ")
    
    # Si el usuario ingresa el texto
    # "salir". Rompemos el ciclo con
    # la palabra *break*
    if edad=="salir":
        print("Saliendo...")
        break

    # Para cada valor ingresado
    edad = int(edad)
    suma_edades += edad
    conteo_edades += 1
    edad_promedio = suma_edades/conteo_edades
    print("Edad promedio: ")
    print(str(edad_promedio))

    if edad_maxima == 0:
        edad_maxima = edad
    else:
        if edad > edad_maxima:
            edad_maxima = edad

    print("Edad máxima: ")
    print(str(edad_maxima))

    if edad_minima == 0:
        edad_minima = edad
    else:
        if edad < edad_minima:
            edad_minima = edad

    print("Edad minima: ")
    print(str(edad_minima))
