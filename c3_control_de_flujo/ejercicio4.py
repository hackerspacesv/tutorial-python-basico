# Este ejemplo calcula las edades promedio
# ingresadas. El programa se repite de forma
# indefinida hasta que el usuario ingresa
# la palabra "salir"

# Tarea: Modifique el programa para que
# muestre la edad máxima y la edad
# mínima ingresada. La edad 0 no se
# considera una edad válida. Puede
# usarla para comparaciones. No se espera
# que el programa entregue una respuesta
# correcta en caso de que el usuario
# ingrese una edad 0
suma_edades = 0
conteo_edades = 0
continuar = True

while continuar:
    edad = input("Ingrese una edad: ")
    
    # Si el usuario ingresa el texto
    # "salir". Rompemos el ciclo con
    # la palabra *break*
    if edad=="salir":
        print("Saliendo...")
        break

    # Para cada valor ingresado
    edad = int(edad)
    suma_edades += edad
    conteo_edades += 1
    edad_promedio = suma_edades/conteo_edades
    print("Edad promedio: ")
    print(str(edad_promedio))
