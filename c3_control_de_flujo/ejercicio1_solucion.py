# Esta es una propuesta de solución
# al ejercicio 1. Recuerda que no hay
# una sola forma de resolver los ejercicios
# pero puedes utilizar este ejemplo como
# referencia del comportamiento esperado
edad = input("Por favor. Ingrese su edad: ")
edad = int(edad)
sexo = input("Ingrese su sexo. (hombre o mujer): ")
# Si esta entre 16 y menos de 18 años puede trabajar
if edad >= 16 and edad < 18:
    print("Puede trabajar con restricciones")
elif edad >=18:
    print("Puede trabajar")
    # Si es mayor de 65 años se puede jubilar
    if sexo == "hombre":
        if edad >= 65:
            print("Se puede jubilar")
    elif sexo == "mujer":
        if edad >= 60:
            print("Se puede jubilar")
else:
    # Si es menor de 16 entonces no pueda trabajar
    print("Usted no puede trabajar")


