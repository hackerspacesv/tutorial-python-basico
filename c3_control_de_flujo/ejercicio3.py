# Este ejmplo muestra el uso de for
# para acceder estructuras de datos
# mas complejas.

# La variable estudiante es una lista
# que contiene diccionarios que describen
# el nombre y la edad de los estudiantes

# En este ejemplo hemos agregado, dentro
# de cada diccionario las notas de las 
# clases que han recibido.

# Tarea: Modifica el programa para mostrar el
# promedio de notas por cada estudiante
# y la nota promedio global para todos
# los estudiantes
estudiantes = [
    {
        "nombre": "Mario", 
        "edad": 34,
        "notas": [7,8.5,9,5,10,7.5]
    },
    {
        "nombre": "Ana", 
        "edad": 28,
        "notas": [8,9,7.5,9]
    },
    {
        "nombre": "Gabriela",
        "edad": 25,
        "notas": [10,8,7,5,9,8.5]
    },
    {
        "nombre": "Jose",
        "edad": 27,
        "notas": [7,7.5,8,9]
    }
]

# La siguiente iteración muestra el nombre
# y la edad para cada uno de los estudiantes
# También muestra el promedio de edades
# para todos los estudiantes
suma_edades = 0
for estudiante in estudiantes:
    print("Nombre: "+estudiante["nombre"])
    print("Edad: "+str(estudiante["edad"]))
    suma_edades += estudiante["edad"]

print("Edad promedio: ")
edad_promedio = suma_edades/len(estudiantes)
print(str(edad_promedio))
