# Este ejemplo demuestra dos formas
# de acceder elementos contenidos dentro
# de una colección (elemento iterable)
edades = [25,15,30,35,40,55,70,23,17,19]

# Acceso por medio de índices
# i toma el valor númerico del índice
print("Acceso por medio de índice")
for i in range(0,len(edades)):
    print("Edad: "+str(edades[i]))

# Acceso por asignación de los elementos
# del objeto iterable.
print("Acceso por asignación directa")
for edad in edades:
    print("Edad: "+str(edad))
