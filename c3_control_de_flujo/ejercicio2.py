# Escriba el código que liste los contenidos
# de cada uno de los elementos iterables
edades = [25,15,30,35,40,55,70,23,17,19]
cadena = "abcdefghijklmnopqrstuvwxyz"
datos_mixtos = ["a",15,2.4,True,34,"f","d"]
tupla = (23,34,32,12,34,3,2,1,6)
diccionario = {
        "llave_1": 2,
        "llave_2": "hola",
        "llave_3": True
        "llave_4": "contenido"
    }

# Ingresa el código después de esta línea
