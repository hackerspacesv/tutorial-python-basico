# Escriba el código que liste los contenidos
# de cada uno de los elementos iterables
edades = [25,15,30,35,40,55,70,23,17,19]
cadena = "abcdefghijklmnopqrstuvwxyz"
datos_mixtos = ["a",15,2.4,True,34,"f","d"]
tupla = (23,34,32,12,34,3,2,1,6)
diccionario = {
        "llave_1": 2,
        "llave_2": "hola",
        "llave_3": True,
        "llave_4": "contenido"
    }

# Ingresa el código después de esta línea
print("Imprimiendo edades")
for i in edades:
    print(str(i))

print("Imprimiendo cadenas")
for i in cadena:
    print(str(i))

print("Imprimiendo datos mixtos")
for i in datos_mixtos:
    print(str(i))

print("Imprimiendo tupla")
for i in tupla:
    print(str(i))

print("imprimiendo diccionario")
for i in diccionario:
    print(str(diccionario[i]))
