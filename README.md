# Tutorial Python Básico

Este repositorio incluye los ejemplos y documentación
que acompaña al Tutorial de Python en el Hackerspace
San Salvador.

## Sesiones

* [Lista de Reproducción con todas las Sesiones](https://www.youtube.com/watch?v=iUMPK-1FoJc&list=PL70aJ5sXpiTO9ksu5mlClFWBEb1N5GfHR)
* [Sesión 1 - Instalando Python](https://www.youtube.com/watch?v=iUMPK-1FoJc):
  en esta sesión explicamos como realizar la instalación de Python para
  Windows.
    * [Descarga Python](https://www.python.org/downloads/)
    * [Ejemplo](./c1_intro/ejemplo1.py)
    * [Ejercicio](./c1_intro/ejercicio1.py)
    * **Tarea**: Investiga cuales son los operadores disponibles en Python.
* [Sesión 2 - Tipos Básicos, Operadores y Colecciones](https://www.youtube.com/watch?v=HAjeA_0eMoE): En esta sesión revisamos la diferencia entre los 
  diferentes tipos de datos disponibles en Python, los distintos tipos
  de operadores y las "colecciones" que permiten agrupar conjuntos de
  datos.
    * **Tarea 1**: Investiga la precedencia de operadores en Python.
    * **Tarea 2**: Lista y clasifica los tipos de operadores disponibles en Python.
* [Sesión 3 - Control de Flujo y Bucles](https://www.youtube.com/watch?v=7g08iTUYtQ8): En esta sesión explicamos cómo utilizar Python para ejecutar código en base a condiciones y explicamos el uso de if-elif-else y los bucles for y while.
    * [Ejemplo 1](./c3_control_de_flujo/ejemplo1.py)
    * [Ejemplo 2](./c3_control_de_flujo/ejemplo2.py)
    * [Ejemplo 3](./c3_control_de_flujo/ejemplo3.py)
    * [Ejemplo 4](./c3_control_de_flujo/ejemplo4.py)
    * **Tarea**: Abre cada uno de los ejercicios e intenta resolver los problemas dados por ti mismo. Se incluye una solución como ejemplo para que puedas revisar si tu solución funciona como se espera. Puedes ejecutarlos con el comando `python ejercicio_solucion.py`. Te recomendamos no ver el código de la solución hasta el final para que tu solución no se vea sesgada por nuestra forma de programar. Recuerda que no hay una sola forma correcta de solucionar los problemas.
        * [Ejercicio 1](./c3_control_de_flujo/ejercicio1.py): Captura el sexo del usuario y muestra el mensaje de jubilación con una menor edad para el caso de las mujeres. [Solución](./c3_control_de_flujo/ejercicio1_solucion.py)
        * [Ejercicio 2](./c3_control_de_flujo/ejercicio2.py): Despliega el contenido de los objetos iterables. [Solución](./c3_control_de_flujo/ejercicio2_solucion.py)
        * [Ejercicio 3](./c3_control_de_flujo/ejercicio3.py): Calcula las notas promedio de los estudiantes. [Solución](./c3_control_de_flujo/ejercicio3_solucion.py)
        * [Ejercicio 3](./c3_control_de_flujo/ejercicio4.py): Muestra la edad mínima y máxima ingresada. [Solución](./c3_control_de_flujo/ejercicio4_solucion.py)
